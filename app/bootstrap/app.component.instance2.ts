import { Component } from '@angular/core';
import {AppComponent} from '../app.component';

@Component({
    selector: 'my-app-instance2',
    templateUrl: './app/bootstrap/app.component.bootstrap.html',
    directives: [AppComponent]

})

export class AppComponent_Instance2 {
    public bootstrappedinstancename: String;
    constructor(){
        this.bootstrappedinstancename = this.constructor.name;
    }
}

