/* Avoid: 'error TS2304: Cannot find name <type>' during compilation */
///<reference path="../typings/index.d.ts"/>
import { bootstrap }    from '@angular/platform-browser-dynamic';

/**
 * Each instance needs to be imported and bootstrapped separately.
 */
import { AppComponent_Instance1 } from './bootstrap/app.component.instance1';
import { AppComponent_Instance2 } from './bootstrap/app.component.instance2';

bootstrap(AppComponent_Instance1);
bootstrap(AppComponent_Instance2);
