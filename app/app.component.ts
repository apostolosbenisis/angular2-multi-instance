/**
 * This is the application's main component that should be shown multiple times on a page.
 */
import { Component , Input} from '@angular/core';

@Component({
    selector: 'my-app',
    templateUrl: './app/app.component.html'
})
export class AppComponent {
    @Input() instancename: String ="Unknown";
}
