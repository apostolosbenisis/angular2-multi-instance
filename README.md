# How to show an Angular2 application multiple times on the same page#

Demonstrate how to instantiate a single page Angular2 applications multiple times on the same html page.


### Installing the demo###
To install the demo, clone this repository, then run 
```
#!javascript

npm install
```
to setup the dependencies.
### Run the demo###
Execute 
```
#!javascript

npm start
```
to compile and launch the demo in the browser.

### How does it work ###
The application's main component that should be shown multiple times on a page is the 'app.component'.
Bootstrapping this multiple times on a page is **not supported** by Angular2:
```
#!javascrpt
import { bootstrap }    from '@angular/platform-browser-dynamic';
import { AppComponent} from './app.component';

bootstrap(AppComponent);
```
```
#!html
<my-app>Bootstrapping... Instance 1</my-app>
<my-app>Bootstrapping... Instance 2</my-app>
```
The above will not work and only the first instance of the application will be shown. The second instance will simply hang.

To bypass this constrain, a wrapper Angular2 application is defined and bootstrapped individually for each instance of the application to be shown on the page. 
Each wrapper uses the application's main component as it's own sub-component and displays it. 
The wrappers follows a simple pattern that allows to instantiate the application's main component like this:  
```
#!javascrpt
import { bootstrap }    from '@angular/platform-browser-dynamic';

import { AppComponent_Instance1 } from './bootstrap/app.component.instance1';
import { AppComponent_Instance2 } from './bootstrap/app.component.instance2';

bootstrap(AppComponent_Instance1);
bootstrap(AppComponent_Instance2);
```
```
#!html
<my-app-instance1>Bootstrapping... Instance 1</my-app-instance1>
<my-app-instance2>Bootstrapping... Instance 1</my-app-instance2>
```

In this demo, wrappers have been created for two instances. If more instances are needed, then create new wrappers by copying one of the existing ones e.g ./app/boostrap/app.component.instance**1**.ts and then replace **1** with the new instance number, e.g **3**, in the file name, selector and class name.

The drawback in this solution is that we need to keep track of the number of instances used in the page and also all wrappers have to be predefined.

**Remark:** Some of the files are based on the Angular 2 tutorial.